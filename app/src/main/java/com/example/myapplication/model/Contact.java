package com.example.myapplication.model;


public class Contact {
    private String name;
    private String email;
    private String address;
    private String phone;
    private int photo;

    public Contact(String name, String email, String address, String phone, int photo) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public int getPhoto() {
        return photo;
    }
}