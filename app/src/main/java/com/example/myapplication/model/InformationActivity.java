package com.example.myapplication.model;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;


public class InformationActivity extends AppCompatActivity {

    ImageView photo;
    TextView name;
    TextView email;
    TextView address;
    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        photo = findViewById(R.id.photo);
        name = findViewById(R.id.txt_name);
        email = findViewById(R.id.txt_email);
        address = findViewById(R.id.txt_address);
        phone = findViewById(R.id.txt_phone);

        Bundle arguments = getIntent().getExtras();

        photo.setImageResource(arguments.getInt("id"));
        name.setText(arguments.get("name").toString());
        email.setText(arguments.get("email").toString());
        address.setText(arguments.get("address").toString());
        phone.setText(arguments.get("phone").toString());
    }
}