package com.example.myapplication.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.OnItemClickListener;
import com.example.myapplication.R;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private Context context;
    private List<Contact> list;
    private OnItemClickListener listener;

    public ContactAdapter(Context context, List<Contact> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.contact, parent, false);
        return new ContactViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Contact contact = list.get(position);
        holder.bind(contact);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder {

        OnItemClickListener listener;
        View root;
        ImageView photo;
        TextView name;
        TextView email;

        public ContactViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            root = itemView.findViewById(R.id.root);
            photo = itemView.findViewById(R.id.photo);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email);
        }

        public void bind(final Contact contact) {
            photo.setImageResource(contact.getPhoto());
            name.setText(contact.getName());
            email.setText(contact.getEmail());
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(contact);
                }
            });
        }
    }
}