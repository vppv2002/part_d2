package com.example.myapplication;

import com.example.myapplication.model.Contact;

public interface OnItemClickListener {
    void onItemClick (Contact contact);
}
