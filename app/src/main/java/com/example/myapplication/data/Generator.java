package com.example.myapplication.data;

import com.example.myapplication.model.Contact;
import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public final class Generator {

    private Generator() {
    }

    public static List<Contact> generate() {
        List<Contact> list = new ArrayList<>();
        list.add(new Contact("Сергей Сергеев", "cergeev@gmail.com", "ул. Сумская 103", "+380981156713", R.drawable.photo_sergeev));
        list.add(new Contact("Никита Воронов", "voronov@gmail.com", "ул. Вальтера 7", "+380500030014", R.drawable.photo_voronov));
        list.add(new Contact("Дима Петров", "petrov@gmail.com", "ул. Чичибабина 34", "+380679754393", R.drawable.photo_petrov));
        return list;
    }
}