package com.example.myapplication.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.model.Contact;
import com.example.myapplication.model.ContactAdapter;
import com.example.myapplication.model.InformationActivity;
import com.example.myapplication.OnItemClickListener;
import com.example.myapplication.R;
import com.example.myapplication.data.Generator;


public class ListFragment extends Fragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        contactAdapter = new ContactAdapter(root.getContext(), Generator.generate(), this);
        recyclerView.setAdapter(contactAdapter);
        return root;}

    @Override
    public void onItemClick (Contact contact){
        Intent intent = new Intent(getActivity(), InformationActivity.class);
        intent.putExtra("id", contact.getPhoto());
        intent.putExtra("name", contact.getName());
        intent.putExtra("email", contact.getEmail());
        intent.putExtra("address", contact.getAddress());
        intent.putExtra("phone", contact.getPhone());
        startActivity(intent);
    }
}
